﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TWD
{
    public class Player
    {
        //Variables that will hold the value of the player's status. 
        //Values will change every time the user clicks on the listbox items.  
        public static int PlayerMorale { get; set; }
        public static int GroupMorale { get; set; }
        public static string PlayerStatus { get; set; }
        public static int GetPlayerStatus(string property)
        {
            switch (property)
            {
                case "playermorale":
                    return PlayerMorale;

                case "groupmorale":
                    return GroupMorale;

                default:
                    return 0;
            }

        }
        //As soon as application is loaded, sets the players status as 100% each
        public static void ResetPlayerProperties()
        {
            PlayerMorale = 70;
            GroupMorale = 70;
            PlayerStatus = "Relaxed";
        }

        //Every time the user clicks on an option, player properties will change according to option value
        //If either playerMorale or GroupMorale has reached below 0% - user has failed the game
        public static int UpdatePlayerStatus(string property, int optionValue)
        {
            
            string playerproperty = property;
            int currentPropertyValue = GetPlayerStatus(playerproperty);
            int result = currentPropertyValue + optionValue;
            if(result >= 101)
            {
                PlayerStatus = "Optimistic";
                result = 100;//If result is higher than 100% will set value to 100%
            }
            else if ((result >= 90) && (result <= 100))
            {
                PlayerStatus = "Optimistic";
            }
            else if ((result >= 70) && (result <= 89))
            {
                PlayerStatus = "Relaxed";
            }
            else if ((result >= 40) && (result <= 69))
            {
                PlayerStatus = "Anxious";
            }
            else if ((result >= 15) && (result <= 39))
            {
                PlayerStatus = "Stressed";
            }
            else if ((result >= 1) && (result <= 14))
            {
                PlayerStatus = "Depressed";
            }
            else if (result <= 0)
            {
                result = 0; //If result is lower than 0% will set value to 0%
                PlayerStatus = "Dead";
                FailedGame(playerproperty, result);
            }
            //MessageBox.Show(random.ToString());
            //MessageBox.Show("Property: " + currentPropertyValue.ToString() + "\n+SValue: " + optionValue.ToString() + "%\nResult: " + result.ToString() + "%\nPlayerStatus: " + PlayerStatus.ToString());
            return result;
        }
        public static string path;
        public static string UpdateStatusImage(int propertyValue)
        {
            path = String.Empty;
            if ((propertyValue >= 90) && (propertyValue <= 100))
            {
                return "Green.png";
            }
            else if ((propertyValue >= 70) && (propertyValue <= 89))
            {
                return "Teal.png";
            }
            else if ((propertyValue >= 40) && (propertyValue <= 69))
            {
                return "Orange.png";
            }
            else if ((propertyValue >= 15) && (propertyValue <= 39))
            {
                return "Red.png";
            }
            else if (propertyValue <= 14)
            {
                return "Dark.png";
            }
            else
            {
                Console.Write("\nNot valid image filepath\nPropertyValue: " + propertyValue + " %");
                return path = String.Empty;
            }

        }


        public static void FailedGame(string playerproperty, int result)
        {
            switch (playerproperty)
            {
                case "playermorale":
                    MessageBox.Show("You Commited Suicide \nPlayer Morale: " + playerproperty);
                    break;

                case "groupmorale":
                    MessageBox.Show("You were Murdered By Group Member \nGroup Morale: " + playerproperty);
                    break;

            }
        }
    }

}
