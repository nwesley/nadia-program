﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static DoubleAnimation brighten = new DoubleAnimation(1, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation darken = new DoubleAnimation(0.6, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation vanish = new DoubleAnimation(0, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation slow = new DoubleAnimation(0.6, TimeSpan.FromSeconds(2));
        public static DoubleAnimation fast = new DoubleAnimation(1, TimeSpan.FromSeconds(1.5));

        //Scale & Translate variables used for mouse-enter events over bubble image buttons.
        public static DoubleAnimation translate_x = new DoubleAnimation() { From = 0, To = -5, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation translate_y = new DoubleAnimation() { From = 0, To = -5, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation increaseScale_x = new DoubleAnimation() { From = 1, To = 1.1, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation increaseScale_y = new DoubleAnimation() { From = 1, To = 1.1, Duration = TimeSpan.FromSeconds(0.5) };

        //Scale & Translate variables used for mouse-leave events over bubble image buttons.
        public static DoubleAnimation return_x = new DoubleAnimation() { From = 0, To = 0, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation return_y = new DoubleAnimation() { From = 0, To = 0, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation decreaseScale_x = new DoubleAnimation() { From = 1, To = 1, Duration = TimeSpan.FromSeconds(0.5) };
        public static DoubleAnimation decreaseScale_y = new DoubleAnimation() { From = 1, To = 1, Duration = TimeSpan.FromSeconds(0.5) };

        public static string userNavigationSelection;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Loading image soruces filepaths
            imgCampfire.Source = new BitmapImage(new Uri(@"/Resources/Images/campfire_dark.png", UriKind.Relative));
            imgConfig.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/tool_inactive.png", UriKind.Relative));
            imgSetting.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/setting_inactive.png", UriKind.Relative));
            imgLeave.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/leave_inactive.png", UriKind.Relative));
            imgHelp.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/help_inactive.png", UriKind.Relative));
            imgGuide.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/guide_inactive.png", UriKind.Relative));
            imgBag.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/bag_inactive.png", UriKind.Relative));

            //Loading image opcaity starts at 0
            imgCampfire.Opacity = 0;
            circle.Opacity = 0;
            imgSetting.Opacity = 0;
            imgLeave.Opacity = 0;
            imgConfig.Opacity = 0;
            imgGuide.Opacity = 0;
            imgHelp.Opacity = 0;
            imgBag.Opacity = 0;

            //Loading image fade animations
            circle.BeginAnimation(Image.OpacityProperty, fast);
            imgCampfire.BeginAnimation(Image.OpacityProperty, slow);
            imgSetting.BeginAnimation(Image.OpacityProperty, slow);
            imgConfig.BeginAnimation(Image.OpacityProperty, slow);
            imgLeave.BeginAnimation(Image.OpacityProperty, slow);
            imgHelp.BeginAnimation(Image.OpacityProperty, slow);
            imgGuide.BeginAnimation(Image.OpacityProperty, slow);
            imgBag.BeginAnimation(Image.OpacityProperty, slow);

        }

        //Campfire Image Button Events & Animations
        private void imgCampfire_MouseEnter(object sender, MouseEventArgs e)
        {
            imgCampfire.Source = new BitmapImage(new Uri(@"/Resources/Images/campfire_bright.png", UriKind.Relative));
            imgCampfire.BeginAnimation(Image.OpacityProperty, brighten);
        }
        private void imgCampfire_MouseLeave(object sender, MouseEventArgs e)
        {
            imgCampfire.Source = new BitmapImage(new Uri(@"/Resources/Images/campfire_dark.png", UriKind.Relative));
            imgCampfire.BeginAnimation(Image.OpacityProperty, darken);
        }
        private void imgCampfire_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundScenario b = new BackgroundScenario();
            b.Show();
            this.Close();
        }


        //Setting Image Button Events & Animations
        private void imgSetting_MouseEnter(object sender, MouseEventArgs e)
        {
            imgSetting.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/setting_active.png", UriKind.Relative));
            imgSetting.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleSetting.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleSetting.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransSetting.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransSetting.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgSetting_MouseLeave(object sender, MouseEventArgs e)
        {
            imgSetting.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/setting_inactive.png", UriKind.Relative));
            imgSetting.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleSetting.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleSetting.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransSetting.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransSetting.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgSetting_MouseDown(object sender, MouseEventArgs e)
        {
            userNavigationSelection = "SettingPage";
            NavigationWindow n = new NavigationWindow();
            n.Show();
            this.Close();
        }


        //ConfigTool Image Button Events & Animations
        private void imgConfig_MouseEnter(object sender, MouseEventArgs e)
        {
            imgConfig.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/tool_active.png", UriKind.Relative));
            imgConfig.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleConfig.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleConfig.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransConfig.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransConfig.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgConfig_MouseLeave(object sender, MouseEventArgs e)
        {
            imgConfig.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/tool_inactive.png", UriKind.Relative));
            imgConfig.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleConfig.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleConfig.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransConfig.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransConfig.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgConfig_MouseDown(object sender, MouseEventArgs e)
        {
            userNavigationSelection = "ConfigPage";
            NavigationWindow n = new NavigationWindow();
            n.Show();
            this.Close();
        }



        //Exit Image Button Events & Animations
        private void imgLeave_MouseEnter(object sender, MouseEventArgs e)
        {
            imgLeave.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/leave_active.png", UriKind.Relative));
            imgLeave.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleLeave.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleLeave.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransLeave.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransLeave.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgLeave_MouseLeave(object sender, MouseEventArgs e)
        {
            imgLeave.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/leave_inactive.png", UriKind.Relative));
            imgLeave.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleLeave.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleLeave.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransLeave.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransLeave.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgLeave_MouseDown(object sender, MouseEventArgs e)
        {
            this.Close();
        }



        //Help Image Button Events & Animations
        private void imgHelp_MouseEnter(object sender, MouseEventArgs e)
        {
            imgHelp.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/help_active.png", UriKind.Relative));
            imgHelp.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleHelp.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleHelp.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransHelp.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransHelp.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgHelp_MouseLeave(object sender, MouseEventArgs e)
        {
            imgHelp.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/help_inactive.png", UriKind.Relative));
            imgHelp.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleHelp.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleHelp.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransHelp.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransHelp.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgHelp_MouseDown(object sender, MouseEventArgs e)
        {
            userNavigationSelection = "HelpPage";
            NavigationWindow n = new NavigationWindow();
            n.Show();
            this.Close();
        }


        //Guide Image Button Events & Animations
        private void imgGuide_MouseEnter(object sender, MouseEventArgs e)
        {
            imgGuide.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/guide_active.png", UriKind.Relative));
            imgGuide.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleGuide.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleGuide.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransGuide.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransGuide.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgGuide_MouseLeave(object sender, MouseEventArgs e)
        {
            imgGuide.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/guide_inactive.png", UriKind.Relative));
            imgGuide.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleGuide.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleGuide.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransGuide.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransGuide.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgGuide_MouseDown(object sender, MouseEventArgs e)
        {
            userNavigationSelection = "GuidePage";
            NavigationWindow n = new NavigationWindow();
            n.Show();
            this.Close();
        }



        //Inventory Image Button Events & Animations
        private void imgBag_MouseEnter(object sender, MouseEventArgs e)
        {
            imgBag.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/bag_active.png", UriKind.Relative));
            imgBag.BeginAnimation(Image.OpacityProperty, brighten);
            imgScaleBag.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScaleBag.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTransBag.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTransBag.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }
        private void imgBag_MouseLeave(object sender, MouseEventArgs e)
        {
            imgBag.Source = new BitmapImage(new Uri(@"/Resources/Icons/buttons/bag_inactive.png", UriKind.Relative));
            imgBag.BeginAnimation(Image.OpacityProperty, darken);
            imgScaleBag.BeginAnimation(ScaleTransform.ScaleXProperty, decreaseScale_x);
            imgScaleBag.BeginAnimation(ScaleTransform.ScaleYProperty, decreaseScale_y);
            imgTransBag.BeginAnimation(TranslateTransform.XProperty, return_x);
            imgTransBag.BeginAnimation(TranslateTransform.YProperty, return_y);
        }
        private void imgBag_MouseDown(object sender, MouseEventArgs e)
        {
            userNavigationSelection = "InventoryPage";
            NavigationWindow n = new NavigationWindow();
            n.Show();
            this.Close();
        }

    }
}

