﻿#pragma checksum "..\..\ScenarioWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "DCAE68BCF0D1D1E856DE20CE6CA923DCA3E21EB8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TWD;


namespace TWD {
    
    
    /// <summary>
    /// ScenarioWindow
    /// </summary>
    public partial class ScenarioWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CustomGrid;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgRedMap;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtbxScenarioContent;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPlayerStatus;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgGstatus;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGroupMorale;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgPstatus;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPlayerMorale;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblScenarioQuestion;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\ScenarioWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbScenarioOptions;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TWD;component/scenariowindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ScenarioWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\ScenarioWindow.xaml"
            ((TWD.ScenarioWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.CustomGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.imgRedMap = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.txtbxScenarioContent = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.lblPlayerStatus = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.imgGstatus = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.lblGroupMorale = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.imgPstatus = ((System.Windows.Controls.Image)(target));
            return;
            case 9:
            this.lblPlayerMorale = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblScenarioQuestion = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lbScenarioOptions = ((System.Windows.Controls.ListBox)(target));
            
            #line 82 "..\..\ScenarioWindow.xaml"
            this.lbScenarioOptions.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.LbScenarioOptions_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

