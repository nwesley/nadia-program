﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Methods to detect resource text file folder locations, read the text file names, and to take the content from the text files
    /// to then be stored within application. 
    /// </summary>

    public class Scenario
    {
        /// <summary>
        /// All information of scenario is stored into this field 'Scenarios'.
        /// Key  : Scenario Code
        /// Value: Scenario Contents (This is string ARRAY which is contained the contents of scenario)
        /// </summary>
        //public static SortedDictionary<string, string[]> Scenarios = new SortedDictionary<string, string[]>();
        public static SortedDictionary<string, ScenarioOptionProperties> Scenarios = new SortedDictionary<string, ScenarioOptionProperties>();
        public static ScenarioWindow IndexMainWindow;
        public static BackgroundScenario BackgroundWindow;
        public static String contents;

        public static string LoadBackground(int number)
        {
            string[] backgroundData = System.IO.File.ReadAllLines(@"Resources\Background.txt");
            string content = backgroundData[number];
            return content;
        }

        public static void LoadScenario()
        {
            SortedDictionary<string, string> fileAndFolderNames;
            fileAndFolderNames = LoadFolderAndFileNames();
            SetScenarios(fileAndFolderNames);
        }

        /// <summary>
        /// Loads folder and file names from LoadData.txt file.
        /// </summary>
        private static SortedDictionary<string, string> LoadFolderAndFileNames()
        {
            // Key  : File name	<= Scenario Code
            // Value: Folder name
            SortedDictionary<string, string> fileAndFolderNames = new SortedDictionary<string, string>();

            // ----------------------------------------------
            // loadData[0] = "S1|S1,S1OP1,S1OP2,S1OP3,S1OP4"
            // loadData[0] = "S2|S2,S2OP1,S2OP2,S2OP3,S2OP4"
            // loadData[0] = "S3|S3,S3OP1,S3OP2,S3OP3,S3OP4"
            // ----------------------------------------------
            string[] loadData = System.IO.File.ReadAllLines(@"Resources\LoadData.txt");
            string[] aFolderAndFileNames;
            string aFolderName;
            string[] fileNames;

            foreach (string item in loadData)
            {
                // ----------------------------------------------
                // aFolderAndFilesName[0] = "S1"
                // aFolderAndFilesName[1] = "S1,S1OP1,S1OP2,S1OP3,S1OP4"
                // ----------------------------------------------
                aFolderAndFileNames = item.Split('|');
                aFolderName = aFolderAndFileNames[0];
                fileNames = aFolderAndFileNames[1].Split(',');

                // ----------------------------------------------
                // (key=fileName, value=folderName)
                // ("S1", "S1")
                // ("S1OP1", "S1")
                // ("S1OP2", "S1")
                // ("S1OP3", "S1")
                // ("S1OP4", "S1")
                // ----------------------------------------------
                foreach (string aFileName in fileNames)
                {
                    fileAndFolderNames.Add(aFileName, aFolderName);
                }
            }

            return fileAndFolderNames;
        }

        /// <summary>
        /// Sets Scenarios information, which are codes and contents, into the field Scenarios.
        /// </summary>
        private static void SetScenarios(SortedDictionary<string, string> foldersAndFilesName)
        {

            string scenarioCode, aFolderName, aFileName;
            string[] contents;

            // Key  : File name	<= Scenario Code
            // Value: Folder name
            foreach (KeyValuePair<string, string> kvp in foldersAndFilesName)
            {
                aFolderName = kvp.Value;
                aFileName = scenarioCode = kvp.Key;

                contents = System.IO.File.ReadAllLines(@"Resources\" + aFolderName + @"\" + aFileName + ".txt");

                ScenarioOptionProperties optionProperties = new ScenarioOptionProperties();

                foreach (string str in contents)
                {
                    // line[0] : title
                    // line[1] : content
                    string[] line = str.Split(new string[] { "[:]" }, StringSplitOptions.None);

                    switch (line[0].ToLower())
                    {
                        case "scontent":
                            optionProperties.SContent = line[1];
                            break;

                        case "question":
                            optionProperties.Question = line[1];
                            break;

                        case "code":
                            optionProperties.Code = line[1];
                            break;

                        case "content":
                            optionProperties.Content = line[1];
                            break;

                        case "consequence":
                            optionProperties.Consequence = line[1];
                            break;

                        case "playermorale":
                            int num1;
                            Int32.TryParse(line[1], out num1);
                            optionProperties.PlayerMorale = num1;
                            break;

                        case "groupmorale":
                            int num2;
                            Int32.TryParse(line[1], out num2);
                            optionProperties.GroupMorale = num2;
                            break;

                        case "perks":
                            optionProperties.Perks = line[1];
                            break;
                    }
                }
                Scenarios.Add(scenarioCode, optionProperties);
            }
        }

        //To get the content from the scenarios (using the code & property strings)
        public static object GetScenarioContent(string code, string property)
        {
            object content = ((ScenarioOptionProperties)Scenarios[code]).GetContent(property);
            return content;
        }

        //To display the properties that the user selected from listbox scenario option as one string
        public static string DisplayOptionSelection(string scene, string code)
        {
            string consequence = (string)GetScenarioContent((scene + code), "consequence");
            string player = (GetScenarioContent((scene + code), "playermorale").ToString());
            string group = (GetScenarioContent((scene + code), "groupmorale").ToString());
            string perks = (string)GetScenarioContent((scene + code), "perks");
            string optionProperties = "Consequence: " + consequence + "\nPlayer Morale: " + player + "%\nGroup Morale: " + group + "%\nPerks: "+ perks;
            MessageBox.Show(optionProperties);
            return optionProperties;
        }

        //To get the int values of the listbox option (positive / negative playerMorale or groupMorale) for calculation
        public static int GetOptionValue(string scene, string code, string property)
        {
            int value = Convert.ToInt32(GetScenarioContent(scene + code, property));
            return value;
        }

    }

    public class ScenarioOptionProperties
    {
        //Scenario Text Files: "S1.txt" contains SContent & Question
        public string SContent { get; set; }
        public string Question { get; set; }

        //Scenario Option Text Files: "S1OP1.txt" contains Content, Consequence, PlayerMorale, GroupMorale & Perks
        public string Code { get; set; }
        public string Content { get; set; }
        public string Consequence { get; set; }
        public int PlayerMorale { get; set; }
        public int GroupMorale { get; set; }
        public string Perks { get; set; }

        public object GetContent(string property)
        {
            switch (property)
            {
                case "scontent":
                    return SContent;

                case "question":
                    return Question;

                case "code":
                    return Code;

                case "content":
                    return Content;

                case "consequence":
                    return Consequence;

                case "playermorale":
                    return PlayerMorale;

                case "groupmorale":
                    return GroupMorale;

                case "perks":
                    return Perks;

                default:
                    return null;
            }
        }
    }
}