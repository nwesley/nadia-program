﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for ScenarioWindow.xaml
    /// </summary>
    public partial class ScenarioWindow : Window
    {
        public String scenarioCode;
        public string g = "g", p = "p";
        //public static string playerFilepath, groupFilepath;
        public ScenarioWindow()
        {
            InitializeComponent();
            Scenario.IndexMainWindow = this;       
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Player.ResetPlayerProperties();
            Scenario.LoadScenario();
            DisplayScene("S1");
        }

        public void DisplayScene(string code)
        {
            string scene = code;
            lblPlayerStatus.Content = "Player Status : " + Player.PlayerStatus;
            lblPlayerMorale.Content = Player.PlayerMorale.ToString() + "%";
            lblGroupMorale.Content = Player.GroupMorale.ToString() + "%";
            imgRedMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage9.png", UriKind.Relative));
            txtbxScenarioContent.Text = (string)Scenario.GetScenarioContent(code, "scontent");
            lblScenarioQuestion.Content = (string)Scenario.GetScenarioContent(code, "question");
            string op1 = (string)Scenario.GetScenarioContent(code + "OP1", "content");
            string op2 = (string)Scenario.GetScenarioContent(code + "OP2", "content");
            string op3 = (string)Scenario.GetScenarioContent(code + "OP3", "content");
            string op4 = (string)Scenario.GetScenarioContent(code + "OP4", "content");
            lbScenarioOptions.Items.Add(new ListBoxItem("OP1", op1, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP2", op2, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP3", op3, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP4", op4, scene));

            string playerFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgPstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/player/" + playerFilepath, UriKind.Relative));

            string groupFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgGstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/group/" + groupFilepath, UriKind.Relative));

            //imgGstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/g/" + groupFilepath, UriKind.Relative));
            //imgPstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/p/" + playerFilepath, UriKind.Relative));
            //Retrieves the value consequence of the selected option for player morale




        }
        public void LbScenarioOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // (After clearing listbox items) This will return if their are no items in the listbox
            if (lbScenarioOptions.Items.Count == 0)
            {
                return;
            }

            ListBoxItem userSelection = (ListBoxItem)lbScenarioOptions.SelectedItem;
            string scene = userSelection.Scene;
            string code = userSelection.Code;

            //Retrieves the value consequence of the selected option for PLAYER MORALE, 
            //      Updates the group status feeling using the playerResult value 
            //          & Updates the group status image using the updaded playerMorale value
            int playerResult = Scenario.GetOptionValue(scene, code, "playermorale");
            Player.PlayerMorale = Player.UpdatePlayerStatus("playermorale", playerResult);
            string playerFilepath = Player.UpdateStatusImage(Player.PlayerMorale);
            imgPstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/player/" + playerFilepath, UriKind.Relative));

            //Retrieves the value consequence of the selected option for GROUP MORALE, 
            //      Updates the group status feeling using the groupResult value 
            //          & Updates the group status image using the updaded GroupMorale value
            int groupResult = Scenario.GetOptionValue(scene, code, "groupmorale");
            Player.GroupMorale = Player.UpdatePlayerStatus("groupmorale", groupResult);
            string groupFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgGstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/group/" + groupFilepath, UriKind.Relative));

            //Fill out the scenario window with content
            string optionProperties = Scenario.DisplayOptionSelection(scene, code);
            lblPlayerMorale.Content = Player.PlayerMorale.ToString() + "%";
            lblGroupMorale.Content = Player.GroupMorale.ToString() + "%";
            lblPlayerStatus.Content = "Player Status : " + Player.PlayerStatus; 
            NextScenario(scene);

        }
        public void ClearScenarioWindow()
        {
            txtbxScenarioContent.Text = String.Empty;
            lblScenarioQuestion.Content = String.Empty;
            imgGstatus.Source = null;
            imgPstatus.Source = null;
            lblPlayerStatus.Content = String.Empty;
            lblPlayerMorale.Content = String.Empty;
            lblGroupMorale.Content = String.Empty;
            //LbScenarioOptions_SelectionChanged is triggered when listbox items are cleared.
            lbScenarioOptions.Items.Clear();
        }
        public void NextScenario(string scene)
        {
            if(scene == "S1")
            {
                ClearScenarioWindow();
                DisplayScene("S2");
            }
            else if (scene == "S2")
            {
                ClearScenarioWindow();
                DisplayScene("S3");
            }
            else if (scene == "S3")
            {
                ClearScenarioWindow();
                DisplayScene("S4");
            }
            if (scene == "S4")
            {
                //Scenario.LoadScenario("S5");
                //ClearScenarioWindow();
                //DisplayScene("S5");
            }
        }
    }
    public class ListBoxItem
    {
        public string Code;
        public string Content;
        public string Scene;

        public ListBoxItem(string code, string content, string scene)
        {
            Code = code;
            Content = content;
            Scene = scene;
        }

        public override string ToString()
        {
            return Content;
        }
    }
}