﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for NavigationWindow.xaml
    /// </summary>
    public partial class NavigationWindow : Window
    {
        public NavigationWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string page = MainWindow.userNavigationSelection;
            frmeNavigation.Source = new Uri(@page+".xaml", UriKind.Relative);
        }
        private void btnInventory_Click(object sender, RoutedEventArgs e)
        {
            string page = "InventoryPage";
            frmeNavigation.Source = new Uri(@page + ".xaml", UriKind.Relative);
        }
        private void btnGuide_Click(object sender, RoutedEventArgs e)
        {
            string page = "GuidePage";
            frmeNavigation.Source = new Uri(@page + ".xaml", UriKind.Relative);
        }
        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            string page = "HelpPage";
            frmeNavigation.Source = new Uri(@page + ".xaml", UriKind.Relative);
        }
        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            string page = "SettingPage";
            frmeNavigation.Source = new Uri(@page + ".xaml", UriKind.Relative);
        }
        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            string page = "ConfigPage";
            frmeNavigation.Source = new Uri(@page + ".xaml", UriKind.Relative);
        }
        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            this.Close();
        }
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            BackgroundScenario b = new BackgroundScenario();
            b.Show();
            this.Close();
        }

    }
}
