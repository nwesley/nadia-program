Content[:]Respond to the distress call and give the group your medicine
Consequence[:]Travelling to the group has depleted your gasoline. By giving away your medicine and getting nothing in return, the majority of the group doesn't approve of your decision. 
PlayerMorale[:]+0
GroupMorale[:]-10
Perk[:]