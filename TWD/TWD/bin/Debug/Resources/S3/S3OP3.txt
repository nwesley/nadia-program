Content[:]Threaten the stranger to leave or else
Consequence[:]The stranger flees into tho forest. You're not sure whether she will keep her promise to never return. Sara doesn't approve of your threats. Michael doesn't approve that you let the stranger leave knowing the location of their camp. 
PlayerMorale[:]-10
GroupMorale[:]-20
Perk[:]Camp Detected